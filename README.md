# devops-netology
Autor: Денис Лукьянов

По результатам файла .gitignoge будут игнорироваться:
Каталоги '.terraform' на всех уровнях вложенности
Файлы с расширениям 'tfstate'
Файлы, которые содержат в имени '.tfstate.'
Файлы, которые заканчиваются на '_override.tf' и '_override.tf.json'
Файлы 'crash.log', 'override.tf', 'override.tf.json', '.terraformrc', 'terraform.rc'
